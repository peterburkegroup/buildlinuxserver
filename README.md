# Build Rapberry Pi

this is my personal setup scripts to install the stuff I typically want on my Raspberry Pi.

To install Raspian on Pi from a Linux machine:

Download the image you want from Raspbian.

Insert SD card, figure out which one it is with this command before and after insert SD card:
```
df-h
```
 unmount it, e.g.:

```
umount /media/peter/rootfs
umount /media/peter/boot
```
then use dd to burn new image, e.g.:
```
time sudo dd bs=4M if='/home/peter/Documents/pi/2019-04-08-raspbian-stretch-lite.img'      of=/dev/sdf
```
Takes 5 mins. for light for me.
Eject, reinsert disk.
In boot volume, type:
```
touch ssh
```
Now you can ssh in headless.
To get wifi setup, copy the wpa_supplicant.conf file to your boot volume for wifi to work on first boot. (Need to insert your SSID and pwd to wpa_supplicant.conf.)

Ssh into your pi. Run
```
sudo raspi-config
```

Set timezone.
Enable camera.
Give 256 M memory to GPU (advanced options).


Get the script to install stuff I want:
```
wget https://gitlab.com/peterburkegroup/buildlinuxserver/-/raw/master/buildpi.sh
```

Run script to install:
```
sudo chmod 777 ~/buildpi.sh; sudo ~/buildpi.sh 2>&1 | tee buildlog.txt 
```

All in one line:
```
wget https://gitlab.com/peterburkegroup/buildlinuxserver/-/raw/master/buildpi.sh; sudo chmod 777 ~/buildpi.sh; sudo ~/buildpi.sh 2>&1 | tee buildlog.txt 
```
