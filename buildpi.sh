#!/bin/bash

# Copyright Peter Burke 8/22/2020

# define functions first


function installstuff {
    echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
    echo "Installing a whole bunch of packages..."

    start_time_installstuff="$(date -u +%s)"

    time sudo apt-get -y update # 1 min   #Update the list of packages in the software center                                   
    time sudo apt-get -y upgrade # 3.5 min
    #time sudo apt-get -y install screen # 0.5 min
    #time sudo apt-get -y install tcptrack # 0 min
    #time sudo apt-get -y install python  # 6 sec

    time sudo apt-get -y install emacs # 4.5 min  (seems you have to run this twice)                                             
    time sudo apt-get -y install emacs # 0 min (seems you have to run this twice)                                             
    time sudo apt-get -y install top # 0.5 min
    time sudo apt-get -y install htop # 0.5 min
    time sudo apt-get -y install wget # 0.5 min
    time sudo apt-get -y install speedtest-cli # 0.5 min



    echo "Done installing a whole bunch of packages..."

    end_time_installstuff="$(date -u +%s)"
    elapsed_installstuff="$(($end_time_installstuff-$start_time_installstuff))"
    echo "Total of $elapsed_installstuff seconds elapsed for installing packages"
    # 38 mins
    
    
}

# Check for any errors, quit if any
check_errors() {
  if ! [ $? = 0 ]
  then
    echo "An error occured! Aborting...."
    exit 1
  fi
}


function fxyz {
    echo "doing function fxyz"
}




#***********************END OF FUNCTION DEFINITIONS******************************

set -x

echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
echo "This will install mavlink router and set it up for you."
echo "See README in 4guav gitlab repo for documentation."


echo "Starting..."

date


start_time="$(date -u +%s)"


installstuff


date

end_time="$(date -u +%s)"

elapsed="$(($end_time-$start_time))"
echo "Total of $elapsed seconds elapsed for the entire process"


echo "Installation is complete. You will want to restart your Pi to make this work."
echo "No further action should be required. Closing..."

